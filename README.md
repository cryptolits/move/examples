# Explorer

### [swap_platform](../../../-/tree/swap_platform)

---
Smart contract that can be used as AMM as Uniswap.

### [lottery](../../../-/tree/lottery)

---
Unique objects lottery campaign smart contract.

### [marketplace](../../../-/tree/marketplace/marketplace)

---
Example of marketplace for unique objects.
